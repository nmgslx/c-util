#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>
#include "ext2.h"
int main(int argc, char*argv[]) {
   if (argc<2) {
      fprintf(stderr,"Usage: %s <ext2-file> [options]\n", argv[0]);
      return 1;
   }

   int fd = open(argv[1], O_RDWR);
   if (fd == -1) {
      fprintf(stderr,"Open file error: %s\n", argv[1]);
      return 1;
   }

   int *map;
   
   size_t filesize = lseek(fd, 0, SEEK_END);
   printf("file: %s\tsize: ",argv[1]);
   if (filesize>1024*1024*1024)
      printf("%.2f GB\n",filesize/1024.0/1024/1024);
   else if (filesize>1024*1024)
      printf("%.2f MB\n",filesize/1024.0/1024);
   else if (filesize>1024)
      printf("%.2f KB\n",filesize/1024.0);
   else
      printf("%ld bytes\n",filesize);

   map = mmap(0, filesize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
   if (map == MAP_FAILED) {
      close(fd);
      fprintf(stderr,"Error mmapping the file");
      exit(EXIT_FAILURE);
   }

   /////////////////////////////////////////////////
   char *option = "help";
   struct ext2_super_block * super_block = (struct ext2_super_block*) map;
   if (argc>=3) option = argv[2];
   if (strcmp(option,"help")==0) {
      fprintf(stderr,"Usage: %s <ext2-file> <options>\n", argv[0]);
      fprintf(stderr,"where options are:\n");
      fprintf(stderr,"\tbg\t Display block groups\n");
      fprintf(stderr,"\tsb\t Display super block info\n");
   }
   else if (strcmp(option,"bg")==0) {
      fprintf(stderr,"%d\n",super_block->s_block_group_nr); 
       
   }
   /////////////////////////////////////////////////

   if (munmap(map, filesize) == -1) {
      fprintf(stderr,"Error un-mmapping the file");
   }
   close(fd);
}
