all: dump-file dump-ext2

dump-file: dump-file.c
	gcc dump-file.c -o dump-file

dump-ext2: dump-ext2.c ext2.h
	gcc dump-ext2.c -o dump-ext2
