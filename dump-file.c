#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[]) {
   if (argc<2) {
      printf("Usage: %s filename [start, [length]]\n", argv[0]);
      exit(1);
   } 
   FILE *fp = fopen(argv[1],"rb");
   int start =0;
   int length =0;

   if (fp==NULL) {
      printf("Cannot open file: %s\n",argv[1]);
      exit(1);
   }
   if (argc>2) {
      if (strncmp(argv[2],"0x",2)==0 || strncmp(argv[2],"0X",2)==0)
         start = (int)strtol(argv[2], NULL, 16);
      else start = atoi(argv[2]);

   }
   if (argc>3) length = atoi(argv[3]);
   printf("Dump file %s from %d to %d\n", argv[1], start, length+start);

   if (start>0) fseek(fp, start, SEEK_SET);
   int nLast = 1;
   int nTotal = 0;
   unsigned char buffer[16], disp[17];
   disp[16]=0;
   int all_zero = 0;
   int all_ff = 0;
   while (nLast>0 && (length==0 || nTotal<length)) {
      nLast = fread(buffer, 1, sizeof(buffer),fp);

      int flg_0 = 0;
      int flg_f = 0;
      int i = 0;
      for (i=0;i<16;i++) {
         if (buffer[i]!=0) flg_0=1;
         if (buffer[i]!=0xff) flg_f=1;
      }

      if (flg_0==0) all_zero++;
      else all_zero=0;

      if (flg_f==0) all_ff++;
      else all_ff=0;
      
      if (all_zero>1 || all_ff>1) {
         nTotal += nLast;
         if (all_zero==2 || all_ff==2) printf("... (all %s)\n", all_zero>1?"00":"FF");
         continue;
      }

      printf("0x%04x(%05d) ",nTotal+start, nTotal+start);
      int j;
      for (j=0;j<nLast;j++,nTotal++) {
         if (nTotal<length || length==0) { 
            if (buffer[j]>=0x20 && buffer[j]<=0x7f) disp[j]=buffer[j];
            else if (buffer[j]==0) disp[j]=' ';
            else disp[j]='.';
            printf("%02x",buffer[j]);
         }
         else {
            printf("  ");
            disp[j]=' ';
         }
         printf("%s",j==7?"-":" ");
      }
      printf(" %s\n",disp);
      
   }

   fclose(fp);
   return 0;
}
