/*
 * Created by Lixin Shi (nmgslx@gmail.com) April 2017 
 * from http://www.nongnu.org/ext2-doc/ext2.html
 */

#ifndef EXT2_H_HEADER
#define EXT2_H_HEADER

struct ext2_super_block {
	unsigned int   s_inodes_count;      /* Total number of inodes, both used and free */
	unsigned int   s_blocks_count;      /* Num of blocks including all used, free and rerved */
	unsigned int   s_r_blocks_count;    /* Num of blocks reserved for usage of super user */
	unsigned int   s_free_blocks_count; /* Num of free blocks, incl reserved, sum of all block groups */
	unsigned int   s_free_inodes_count; /* Num of free inodes, sum of all the block groups */
	unsigned int   s_first_data_block;  /* First Data Block, i.e., the block containing the superblock structure */
	unsigned int   s_log_block_size;    /* Block size = 1024<<s_log_block_size; 0=1KN 1=2KB ... */
	unsigned int   s_log_frag_size;     /* Fragment size */
	unsigned int   s_blocks_per_group;  /* Total number of blocks per group */
	unsigned int   s_frags_per_group;   /* Total fragments per group */
	unsigned int   s_inodes_per_group;  /* Total inodes per group */
	unsigned int   s_mtime;             /* Last time ithe file system was mounted*/
	unsigned int   s_wtime;             /* Last write access to the fiel system */
	unsigned short s_mnt_count;         /* How many time the fs was mounted since last time it was verified */
	unsigned short s_max_mnt_count;     /* Max num of times that fs may be mounted before a full check */
	unsigned short s_magic;             /* Magic signature: 0xEF53 */
	unsigned short s_state;             /* File system state: EXT2_VALID_FS(1), EXT2_ERROR_FS(2) */
	unsigned short s_errors;            /* Behaviour when detecting errors: CONTINUE(1), RO(2), PANIC(3) */
	unsigned short s_minor_rev_level;   /* minor revision level */
	unsigned int   s_lastcheck;         /* Unix time of last fs check */
	unsigned int   s_checkinterval;     /* max. time interval between fs checks */
	unsigned int   s_creator_os;        /* OS: LINUX(0), HURD(1), MASIX(2), FREEBSD(3), LITES(4) */
	unsigned int   s_rev_level;         /* Revision level: GOOD_OLD_REV(0), DYNAMIC_REV(1) */
	unsigned short s_def_resuid;        /* Default uid for reserved blocks */
	unsigned short s_def_resgid;        /* Default gid for reserved blocks */

	/* EXT2_DYNAMIC_REV Specific */
	unsigned int   s_first_ino;         /* First non-reserved inode */
	unsigned short s_inode_size;        /* size of inode structure */
	unsigned short s_block_group_nr;    /* block group number hosting this superblock */
	unsigned int   s_feature_compat;    /* compatible feature set */

	unsigned int   s_feature_incompat;  /* incompatible feature set */
	unsigned int   s_feature_ro_compat; /* readonly-compatible feature set */
	unsigned char  s_uuid[16];          /* 128-bit value used as the volume id */
	char           s_volume_name[16];   /* 16 bytes volume name (mostly unused) */
	char           s_last_mounted[64];  /* 64 bytes directory path where last mounted */
	unsigned int   s_algorithm_usage_bitmap; /* For compression */

	/* Performance hints. */
	unsigned char  s_prealloc_blocks;     /* Number of blocks to try to preallocate*/
	unsigned char  s_prealloc_dir_blocks; /* Number to preallocate for dirs */
	unsigned short s_padding1;

	/* Journaling support */
	unsigned char  s_journal_uuid[16]; /* uuid of journal superblock */
	unsigned int   s_journal_inum;     /* inode number of journal file */
	unsigned int   s_journal_dev;      /* device number of journal file */
	unsigned int   s_last_orphan;      /* start of list of inodes to delete */

        /* DirectoryIndexing Support */
	unsigned int   s_hash_seed[4];     /* 4x32bit values of hash seed */
	unsigned char  s_def_hash_version; /* Default hash version to use */
	unsigned char  s_reserved_char_pad;
	unsigned short s_reserved_word_pad;

	/* Other options */
	unsigned int   s_default_mount_opts;
	unsigned int   s_first_meta_bg; /* First metablock block group */
	unsigned int   s_reserved[190]; /* Padding to the end of the block */
};


/*
 * Structure of a blocks group descriptor
 */
struct ext2_group_desc
{
	unsigned int   bg_block_bitmap;      /* Blocks bitmap block */
	unsigned int   bg_inode_bitmap;      /* Inodes bitmap block */
	unsigned int   bg_inode_table;       /* Inodes table block */
	unsigned short bg_free_blocks_count; /* Free blocks count for the represented group*/
	unsigned short bg_free_inodes_count; /* Free inodes count for the represented group*/
	unsigned short bg_used_dirs_count;   /* Directories count for the represented group*/
	unsigned short bg_pad;
	unsigned int   bg_reserved[3];
};


/*
 * Structure of an inode on the disk
 */
struct ext2_inode {
	unsigned short i_mode;        /* The format of the described file and access rigts */
	unsigned short i_uid;         /* 16bit user id associated with the file */
	unsigned int   i_size;        /* Size in bytes */
	unsigned int   i_atime;       /* Access time - seconds since 1970-01-01 */
	unsigned int   i_ctime;       /* Creation time - seconds since 1970-01-01 */
	unsigned int   i_mtime;       /* Modification time */
	unsigned int   i_dtime;       /* Deletion Time */
	unsigned short i_gid;         /* Low 16 bits of Group Id */
	unsigned short i_links_count; /* Links count */
	unsigned int   i_blocks;      /* Total number of 512-bytes blocks to contain the data of this inode*/
	unsigned int   i_flags;       /* File flags */
	unsigned int   osd1;          /* OS dependent 1 */
	unsigned int   i_block[15];   /* Pointers to blocks: [0-11]direct, [12]indirect, [13]double-indirect, [14]triply-indirect */
	unsigned int   i_generation;  /* File version (for NFS) */
	unsigned int   i_file_acl;    /* File ACL */
	unsigned int   i_dir_acl;     /* Directory ACL */
	unsigned int   i_faddr;       /* Fragment address */
	unsigned int   extra[3];
};

/*
 * The new version of the directory entry.  Since EXT2 structures are
 * stored in intel byte order, and the name_len field could never be
 * bigger than 255 chars, it's safe to reclaim the extra byte for the
 * file_type field.
 */
struct ext2_dir_entry {
	unsigned int   inode;     /* Inode number */
	unsigned short rec_len;   /* Directory entry length */
	unsigned char  name_len;  /* Name length */
	unsigned char  file_type;
	char           name[];    /* File name, up to EXT2_NAME_LEN */
};


/* Type field for file mode */
#define EXT2_S_IFSOCK	0xC000	/* socket */
#define EXT2_S_IFLNK	0xA000	/* symbolic link */
#define EXT2_S_IFREG	0x8000	/* regular file */
#define EXT2_S_IFBLK	0x6000	/* block device */
#define EXT2_S_IFDIR	0x4000	/* directory */
#define EXT2_S_IFCHR	0x2000	/* character device */
#define EXT2_S_IFIFO	0x1000	/* fifo */
/* process execution user/group override */
#define EXT2_S_ISUID	0x0800	/* Set process User ID */
#define EXT2_S_ISGID	0x0400	/* Set process Group ID */
#define EXT2_S_ISVTX	0x0200	/* sticky bit */
/* access rights */
#define EXT2_S_IRUSR	0x0100	/* user read */
#define EXT2_S_IWUSR	0x0080	/* user write */
#define EXT2_S_IXUSR	0x0040	/* user execute */
#define EXT2_S_IRGRP	0x0020	/* group read */
#define EXT2_S_IWGRP	0x0010	/* group write */
#define EXT2_S_IXGRP	0x0008	/* group execute */
#define EXT2_S_IROTH	0x0004	/* others read */
#define EXT2_S_IWOTH	0x0002	/* others write */
#define EXT2_S_IXOTH	0x0001	/* others execute */

/* Root inode */
#define    EXT2_ROOT_INO         2
/* First non-reserved inode for old ext2 filesystems */
#define EXT2_GOOD_OLD_FIRST_INO 11

/* Structure of a directory entry */
#define EXT2_NAME_LEN 255

/* File system states */
#define	EXT2_VALID_FS			0x0001	/* Unmounted cleanly */
#define	EXT2_ERROR_FS			0x0002	/* Errors detected */

/* Behaviour when detecting errors */
#define EXT2_ERRORS_CONTINUE		1	/* Continue execution */
#define EXT2_ERRORS_RO			2	/* Remount fs read-only */
#define EXT2_ERRORS_PANIC		3	/* Panic */
#define EXT2_ERRORS_DEFAULT		EXT2_ERRORS_CONTINUE

/* Codes for operating systems */
#define EXT2_OS_LINUX		0
#define EXT2_OS_HURD		1
#define EXT2_OS_MASIX		2
#define EXT2_OS_FREEBSD		3
#define EXT2_OS_LITES		4

/* Revision levels */
#define EXT2_GOOD_OLD_REV	0	/* The good old (original) format */
#define EXT2_DYNAMIC_REV	1 	/* V2 format w/ dynamic inode sizes */
#define EXT2_CURRENT_REV	EXT2_GOOD_OLD_REV
#define EXT2_MAX_SUPP_REV	EXT2_DYNAMIC_REV
#define EXT2_GOOD_OLD_INODE_SIZE 128

/* s_feture_compat values */
#define EXT2_FEATURE_COMPAT_DIR_PREALLOC	0x0001
#define EXT2_FEATURE_COMPAT_IMAGIC_INODES	0x0002
#define EXT3_FEATURE_COMPAT_HAS_JOURNAL		0x0004
#define EXT2_FEATURE_COMPAT_EXT_ATTR		0x0008
#define EXT2_FEATURE_COMPAT_RESIZE_INO		0x0010
#define EXT2_FEATURE_COMPAT_DIR_INDEX		0x0020
#define EXT2_FEATURE_COMPAT_ANY			0xffffffff

/* s_feature_ro_compat Values */
#define EXT2_FEATURE_RO_COMPAT_SPARSE_SUPER	0x0001  /* only putting backups in groups of 0, 1 and powers of 3, 5 and 7. */
#define EXT2_FEATURE_RO_COMPAT_LARGE_FILE	0x0002
#define EXT2_FEATURE_RO_COMPAT_BTREE_DIR	0x0004
#define EXT2_FEATURE_RO_COMPAT_ANY		0xffffffff

/* s_feature_incompat Values */
#define EXT2_FEATURE_INCOMPAT_COMPRESSION	0x0001
#define EXT2_FEATURE_INCOMPAT_FILETYPE		0x0002
#define EXT3_FEATURE_INCOMPAT_RECOVER		0x0004
#define EXT3_FEATURE_INCOMPAT_JOURNAL_DEV	0x0008
#define EXT2_FEATURE_INCOMPAT_META_BG		0x0010
#define EXT2_FEATURE_INCOMPAT_ANY		0xffffffff

/* s_algo_bitmap Values */
#define EXT2_LZV1_ALG		0
#define EXT2_LZRW3A_ALG 	1
#define EXT2_GZIP_ALG		2
#define EXT2_BZIP2_ALG		3
#define EXT2_LZO_ALG		4

/* Defined Reserved Inodes */
#define EXT2_BAD_INO		1	/* bad blocks inode */
#define EXT2_ROOT_INO		2	/* root directory inode */
#define EXT2_ACL_IDX_INO	3	/* ACL index inode (deprecated?) */
#define EXT2_ACL_DATA_INO	4	/* ACL data inode (deprecated?) */
#define EXT2_BOOT_LOADER_INO	5	/* boot loader inode */
#define EXT2_UNDEL_DIR_INO	6	/* undelete directory inode */

/* Defined i_flags Values */
#define EXT2_SECRM_FL		0x00000001	/* secure deletion */
#define EXT2_UNRM_FL		0x00000002	/* record for undelete */
#define EXT2_COMPR_FL		0x00000004	/* compressed file */
#define EXT2_SYNC_FL		0x00000008	/* synchronous updates */
#define EXT2_IMMUTABLE_FL	0x00000010	/* immutable file */
#define EXT2_APPEND_FL		0x00000020	/* append only */
#define EXT2_NODUMP_FL		0x00000040	/* do not dump/delete file */
#define EXT2_NOATIME_FL		0x00000080	/* do not update .i_atime */
/* Reserved for compression usage */
#define EXT2_DIRTY_FL		0x00000100	/* Dirty (modified) */
#define EXT2_COMPRBLK_FL	0x00000200	/* compressed blocks */
#define EXT2_NOCOMPR_FL		0x00000400	/* access raw compressed data */
#define EXT2_ECOMPR_FL		0x00000800	/* compression error */
/* End of compression flags */
#define EXT2_BTREE_FL		0x00001000	/* b-tree format directory */
#define EXT2_INDEX_FL		0x00001000	/* hash indexed directory */
#define EXT2_IMAGIC_FL		0x00002000	/* AFS directory */
#define EXT3_JOURNAL_DATA_FL	0x00004000	/* journal file data */
#define EXT2_RESERVED_FL	0x80000000	/* reserved for ext2 library */


/* Defined Inode File Type Values */
#define EXT2_FT_UNKNOWN		0	/* Unknown File Type */
#define EXT2_FT_REG_FILE	1	/* Regular File */
#define EXT2_FT_DIR		2	/* Directory File */
#define EXT2_FT_CHRDEV		3	/* Character Device */
#define EXT2_FT_BLKDEV		4	/* Block Device */
#define EXT2_FT_FIFO		5	/* Buffer File */
#define EXT2_FT_SOCK		6	/* Socket File */
#define EXT2_FT_SYMLINK		7	/* Symbolic Link */


/*
 * Mount flags
 */
#define EXT2_MOUNT_CHECK		0x000001  /* Do mount-time checks */
#define EXT2_MOUNT_OLDALLOC		0x000002  /* Don't use the new Orlov allocator */
#define EXT2_MOUNT_GRPID		0x000004  /* Create files with directory's group */
#define EXT2_MOUNT_DEBUG		0x000008  /* Some debugging messages */
#define EXT2_MOUNT_ERRORS_CONT		0x000010  /* Continue on errors */
#define EXT2_MOUNT_ERRORS_RO		0x000020  /* Remount fs ro on errors */
#define EXT2_MOUNT_ERRORS_PANIC		0x000040  /* Panic on errors */
#define EXT2_MOUNT_MINIX_DF		0x000080  /* Mimics the Minix statfs */
#define EXT2_MOUNT_NOBH			0x000100  /* No buffer_heads */
#define EXT2_MOUNT_NO_UID32		0x000200  /* Disable 32-bit UIDs */
#define EXT2_MOUNT_XATTR_USER		0x004000  /* Extended user attributes */
#define EXT2_MOUNT_POSIX_ACL		0x008000  /* POSIX Access Control Lists */
#define EXT2_MOUNT_XIP			0x010000  /* Obsolete, use DAX */
#define EXT2_MOUNT_USRQUOTA		0x020000  /* user quota */
#define EXT2_MOUNT_GRPQUOTA		0x040000  /* group quota */
#define EXT2_MOUNT_RESERVATION		0x080000  /* Preallocation */
#ifdef CONFIG_FS_DAX
#define EXT2_MOUNT_DAX			0x100000  /* Direct Access */
#else
#define EXT2_MOUNT_DAX			0
#endif

#endif
